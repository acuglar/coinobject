const coin = {
    state: 0,
    flip: function () {  //Randomicamente configura a propriedade “state” para 0 ou 1
        return this.state = Math.round(Math.random())  //"this.state" para acessar a propriedade "state" neste objeto.
    },
    toString: function () {
        if (this.state) {
            return "Tails"
        }
        return "Heads"

    },
    toHTML: function () {
        const image = document.createElement('img');

        if (this.state) {
            image.src = "./images/Cara.jpg"
        } else {
            image.src = "./images/Koroa.jpg"
        }
        return image;
    }
};

function createHTML() {
    let title = document.createElement('h1')
    title.innerText = 'Coin Object'

    let boxContentParagraph = document.createElement('div')
    boxContentParagraph.id = "boxParagraph"

    let boxContentImages = document.createElement('div')
    boxContentImages.id = "boxImages"

    let boxContent = document.createElement('div')
    boxContent.id = "boxContent"

    document.body.appendChild(title)
    document.body.appendChild(boxContent)
    boxContent.appendChild(boxContentParagraph)
    boxContent.appendChild(boxContentImages)
}
createHTML()

function display20Flips() {  //para 20 arremossos, display resultado de cada lançamento como um array de strings.
    const boxContentParagraph = document.querySelector('#boxParagraph')

    for (let i = 1; i <= 20; i++) {
        const stringResult = document.createElement('p')
        coin.flip()
        stringResult.innerText = `Lance ${i}: ${coin.toString()}`
        boxContentParagraph.appendChild(stringResult)
    }

}
display20Flips()

function display20Images() {  //para 20 arremossos, display resultado de cada lançamento como um array de images.
    const boxContentImages = document.querySelector('#boxImages')
    
    for (let i = 1; i <= 20; i++) {
        coin.flip()
        boxContentImages.appendChild(coin.toHTML())
    }

}
display20Images()
